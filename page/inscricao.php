<?php include_once(__DIR__ . "/../style_include.php") ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="description" content="Seja bem-vindo ao site da 7ª edição da SEIFPI. Este é um evento organizado anualmente pelos alunos do curso Técnico em Informática do IFPI Campus Parnaíba.">
    <meta property="og:image" content="../img/previa.png"/>
    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../img/icon.png">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="stylesheet" href="../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="./index.php"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./index.php#sobre">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./index.php#confirmados">Confirmados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./index.php#cronograma">Cronograma</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./index.php#lugar">Lugar</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#"><i class="fas fa-users"></i> Participe!</a>
          </li>
        </ul>
        <ul class="nav justify-content-end social">
          <li class="nav-item">
            <a class="nav-link" href="https://www.instagram.com/seifpi2018/" target="_blank"><i class="fab fa-instagram"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.facebook.com/viiseifpi/" target="_blank"><i class="fab fa-facebook"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <section>

      <article id="inscricao">

        <h1>Aguarde!</h1>
        <h2>Estamos preparando tudo para você...</h2>
        <img src="../img/loading.gif" alt="">

      </article>

    </section>

    <footer>
      <div class="footer-content">

        <div class="footer-subcontent">

          <img src="../img/ifpi-vertical.png" alt="" class="logo-ifpi">

          <article>
            <h1 class="titulo-rodape">Local do evento</h1>
            <p>Rodovia BR-402, Km 3, s/n</p>
            <p>Baixa do Aragão</p>
            <p>Parnaíba - PI</p>
            <p>64215-000</p>
          </article>

          <article>
            <h1 class="titulo-rodape">Contato</h1>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99539-1598</p>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99576-2787</p>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99585-6340</p>
            <p><i class="fas fa-envelope"></i> &nbsp; seifpi.phb@gmail.com</p>
          </article>

        </div>

        <hr>
        <p class="creditos">
          SEIFPI&copy; 2018 · Todos os direitos reservados · Desde 2011 · <i class="fas fa-code"></i> with <i class="fas fa-heart"></i> by <strong><a href="https://mirtilocreation.herokuapp.com" target="_blank" class="mirtilo">Mirtilo Creation</a></strong>
        </p>

      </div>
    </footer>


  </body>
</html>
