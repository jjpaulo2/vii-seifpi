<?php include_once(__DIR__ . "/../style_include.php") ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="description" content="Seja bem-vindo ao site da 7ª edição da SEIFPI. Este é um evento organizado anualmente pelos alunos do curso Técnico em Informática do IFPI Campus Parnaíba.">
    <meta property="og:image" content="../img/previa.png"/>
    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../img/icon.png">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="stylesheet" href="../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#cabecalho"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#sobre">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#confirmados">Confirmados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#cronograma">Cronograma</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#lugar">Lugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../certificados/"><i class="fas fa-calendar-check"></i> Certificados</a>
          </li>
        </ul>
        <ul class="nav justify-content-end social">
          <li class="nav-item">
            <a class="nav-link" href="https://www.instagram.com/seifpi2018/" target="_blank"><i class="fab fa-instagram"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.facebook.com/viiseifpi/" target="_blank"><i class="fab fa-facebook"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <span class="link-nav" id="cabecalho"></span>
    <header>
      <div class="cabecalho-fundo"></div>
      <div class="cabecalho-conteudo">
        <img class="logo-seifpi" src="../img/logo-grande.png" alt="SEIFPI LOGO">
        <h2 class="titulo2">Semana de Informática</h2>
        <h1 class="titulo1">VII SEIFPI</h1>
        <h3 class="titulo3">IFPI Campus Parnaíba</h3>
        <h3 class="data">Dias 12, 13 e 14 de setembro de 2018</h3>
      </div>
    </header>

    <section>

      <article>

        <h1 class="obrigado">Obrigado a todos os participantes!</h1>
        <p class="obrigado-sub">Ficamos muito felizes em saber que todos os participantes gostaram do evento. Este foi um evento feito com muito carinho para todos. Veja a seguir algumas fotos desta edição do evento.</p>

        <div id="slide" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#slide" data-slide-to="0" class="active"></li>
            <li data-target="#slide" data-slide-to="1"></li>
            <li data-target="#slide" data-slide-to="2"></li>
            <li data-target="#slide" data-slide-to="3"></li>
            <li data-target="#slide" data-slide-to="4"></li>
            <li data-target="#slide" data-slide-to="5"></li>
            <li data-target="#slide" data-slide-to="6"></li>
            <li data-target="#slide" data-slide-to="7"></li>
            <li data-target="#slide" data-slide-to="8"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="../img/fotos2018/f1.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f2.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f3.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f4.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f5.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f6.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f7.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f8.jpeg" alt="">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="../img/fotos2018/f9.jpeg" alt="">
            </div>
          </div>
          <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <br>
        <a href="../certificados" class="btn btn-success certificado-botao">GERAR MEU CERTIFICADO</a>

      </article>

      <span class="link-nav" id="sobre"></span>
      <article class="">
        <h1 class="titulo-artigo">Sobre o evento</h1>
        <hr class="linha-titulo">
        <p>A Semana de Informática do IFPI é realizada anualmente pelos alunos do técnico integrado do campus Parnaíba e tem como objetivo promover minicursos, palestras e debates voltados a área da computação na comunidade parnaibana e cidades adjacentes. O evento consta com seis edições bem-sucedidas se caracterizando com um dos únicos eventos tecnológicos na cidade de Parnaíba.</p>
        <p>O evento chega em sua 7ª edição ganhando espaço e reconhecimento no litoral piauiense com conhecimentos atualizados na área de informática voltado ao mercado profissional e antenado com as tendências tecnológicas, promovendo espaços de diálogos com profissionais renomados da região, minicursos sobre tecnologias populares, palestras relacionadas a realidade do mercado nacional, campeonatos de programação e jogos eletrônicos.</p>

        <h2 class="subtitulo-artigo">Fotos da edição 2017</h2>
        <hr>
        <div class="container fotos">
          <div class="row">
            <div class="col-sm">
              <div class="foto f1">
              </div>
            </div>
            <div class="col-sm">
              <div class="foto f2">
              </div>
            </div>
            <div class="col-sm">
              <div class="foto f3">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm">
              <div class="foto f4">
              </div>
            </div>
            <div class="col-sm">
              <div class="foto f5">
              </div>
            </div>
            <div class="col-sm">
              <div class="foto f6">
              </div>
            </div>
          </div>
        </div>
      </article>

      <span class="link-nav" id="confirmados"></span>
      <article class="">
        <h1 class="titulo-artigo">Confirmados</h1>
        <hr class="linha-titulo">
        <div class="container confirmados">
          <div class="row">
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/clodoaldo.jpg" alt="Prof. Me. Clodoaldo Brasilino">
              <h2 class="confirmado-nome">Prof. Me. Clodoaldo Brasilino</h2>
              <hr>
              <p class="confirmado-curriculo">Professor de Informática do IFPI Campus Parnaíba. Bacharel e Mestre em Ciência da Computação pela Universidade Federal da Paraíba. Atua principalmente nas áreas de hardware, infraestrutura e redes de computadores. Instrutor e principal contato da Cisco NetAcademy em Parnaíba.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/gerson.png" alt="Prof. Me. F. Gerson Meneses">
              <h2 class="confirmado-nome">Prof. Me. F. Gerson Meneses</h2>
              <hr>
              <p class="confirmado-curriculo">Professor de Informática do IFPI - Campus Parnaíba. Doutorando em Biotecnologia pela Rede Nordeste de Biotecnologia, Mestre em Ciência da Computação pela Universidade Federal Fluminense, Especialista em Análise de Sistemas e em Banco de Dados, Bacharel em Ciência da Computação.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/sekeff.png" alt="Prof. Dr. Ricardo Sekeff">
              <h2 class="confirmado-nome">Prof. Dr. Ricardo Sekeff</h2>
              <hr>
              <p class="confirmado-curriculo">Docente do IFPI Campus Piripiri. Possui graduação em Ciências da Computação pela Universidade Estadual do Ceará (2000), mestrado em Maîtrise en Gestion des Organisations - Université du Québec à Chicoutimi (2003) e Doutorado pelo Instituto de Matemática e Estatística (IME) da Universidade de São Paulo (USP).</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/gildario.png" alt="Prof. Dr. Gildário Lima">
              <h2 class="confirmado-nome">Prof. Dr. Gildário Lima</h2>
              <hr>
              <p class="confirmado-curriculo">Fundador do Instituto de Tecnologia, Inovação e Ciências do Delta (2015), presidente do Conselho Estadual da Olimpíada Brasileira de Robótica, diretor de Pesquisa e Desenvolvimento da Organização não Governamental Cajuína Tech, colaborador do Laboratório de Neuro-Inovação Tecnológica & Mapeamento Cerebral da UFPI e fundador do Método TRON.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/henrique.jpg" alt="Prof. Henrique Fontenele">
              <h2 class="confirmado-nome">Prof. Henrique Fontenele</h2>
              <hr>
              <p class="confirmado-curriculo">Docente da Faculdade Mauricio de Nassau - Parnaíba. Possui graduação em Sistema de Informação pelo Instituto de Estudos Superiores da Amazônia (2009), especialização em Teste de Software pelo Instituto Euro-Americano de Educação, Ciência e Tecnologia (2011) e curso-tecnico-profissionalizante pela Escola Técnica Estadual Ministro Petrônio Portela (1999).</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/gustavo.jpg" alt="Gustavo Carvalho">
              <h2 class="confirmado-nome">Gustavo Carvalho</h2>
              <hr>
              <p class="confirmado-curriculo">Co-fundador do PUG Piauí. Tecnólogo em Análise e Desenvolvimento de Sistemas pela Universidade Estácio de Sá. Programador Python e Django experiente. Engenheiro de Software na Nuveo SmartCloud.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/patrik.jpg" alt="João Patrick Rodrigues">
              <h2 class="confirmado-nome">João Patrick Rodrigues</h2>
              <hr>
              <p class="confirmado-curriculo">Técnico em Informática pelo IFPI Campus Parnaíba. Bacharelando em Sistemas de Informação pela Faculdade Maurício de Nassau, UNINASSAU. Atualmente trabalha na empresa TDA Informática como programador mobile com tecnologias híbridas e nativas, desenvolvedor de sites com Joomla e suporte de hardware.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/rodrigo.jpg" alt="Luiz Rodrigo Gonzaga">
              <h2 class="confirmado-nome">Luiz Rodrigo Gonzaga</h2>
              <hr>
              <p class="confirmado-curriculo">Bacharelando em Ciência da Computação pela Universidade Estadual do Piauí. Organizador e colaborador do PUG Parnaíba. Desenvolvedor Python e Django experiente. Trabalhou como Freelancer na empresa Colins. Atualmente estagia na empresa PVP.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/werberth.jpg" alt="Werberth Vinicius">
              <h2 class="confirmado-nome">Werberth Vinicius</h2>
              <hr>
              <p class="confirmado-curriculo">Bacharelando em Ciência da Computação pela Universidade Estadual do Piauí. Organizador e colaborador do PUG Parnaíba. Desenvolvedor Web na Midiacode. Experiente no desenvolvimento de APIs utilizando Python e Django.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/alessandro.png" alt="Alessandro Saraiva">
              <h2 class="confirmado-nome">Alessandro Saraiva</h2>
              <hr>
              <p class="confirmado-curriculo">Diretor Administrativo na empresa PVP S/A, Professor em Pós-Graduação na Uninassau, Bacharel em Ciência da Computação pela UESPI, Especialista em Banco de Dados pelo IFPI, Especialista em MBA Executivo e Gestão Empresarial, Especialisando em Business Data Science, Mestrando em Engenharia da Informática pela Universidade de Trás-os-Montes em Alto Douro.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/daniel.jpg" alt="Daniel Farias">
              <h2 class="confirmado-nome">Daniel Farias</h2>
              <hr>
              <p class="confirmado-curriculo">Técnico em Informática pelo IFPI Campus Parnaíba. Graduando do curso de Tecnologia em Análise e Desenvolvimento de Sistemas pelo IFPI Campus Teresina Central, ex-aluno do Fox Valley Technical College (EUA). Integrante do LIMS (Laboratory of Innovation on Multimedia Systems). Desenvolvedor de Software com experiência em tecnologias como Javascript, Python e PHP.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/mazulo.jpg" alt="Patrick Mazulo">
              <h2 class="confirmado-nome">Patrick Mazulo</h2>
              <hr>
              <p class="confirmado-curriculo">Técnico em Informática pelo CEEPMPP, quase Bacharel em Sistemas de Informação pela Faculdade Maurício de Nassau. Trabalha com desenvolvimento web desde 2011, tendo passado por empresas brasileiras e americanas. Atualmente trabalha como "Software Engineer" numa empresa do Vale do Silício. Usuário e compartilhador da palavra do Python, passa a maior parte do seu tempo estudando tecnologias, assim como maneiras de aplicá-las no mundo real.</p>
            </div>
            <div class="col-sm">
              <img class="confirmado-foto" src="../img/confirmados/ana-maria.jpg" alt="Ana Maria">
              <h2 class="confirmado-nome">Ana Maria</h2>
              <hr>
              <p class="confirmado-curriculo">Co-organizadora da Pyladies Teresina desde 2016. Quase física, estudante do 7º período do curso na Universidade Federal do Piauí. Atua na área da física teórica com o Estudo das Propriedades Mecânicas de Nanotubos de Carbono com deformações estruturais. Já palestrou em eventos como a Python Nordeste, maior encontro da comunidade python nordestina, onde falou sobre o uso do python em aplicações da física.</p>
            </div>
          </div>
        </div>
      </article>

      <span class="link-nav" id="cronograma"></span>
      <article>
        <h1 class="titulo-artigo">Cronograma</h1>
        <hr class="linha-titulo">
        <div id="cronograma-content">
          <img src="../img/cronograma.png" id="tabela-cronograma" alt="">
        </div>
      </article>

      <article>

        <h1 class="titulo-artigo">Mesa Redonda</h1>
        <hr class="linha-titulo">

        <h2 class="titulo-mesa">"IoT e a Quarta Revolução Industrial"</h3>
        <p>Fique por dentro das perspectivas acadêmicas e profissionais para a IoT e entenda os principais impactos que a revolução, que para alguns especialistas já é a Quarta Revolução Industrial, iniciada por esse mundo causou e continua causando no nosso dia a dia.</p>

        <h2 class="subtitulo-artigo">Compondo a mesa</h2>
        <hr>
        <img src="../img/mesa/alessandro.svg" alt="Alessandro Saraiva" class="integrante aless">
        <img src="../img/mesa/clodoaldo.svg" alt="Prof. Me. Clodoaldo Brasilino" class="integrante clod">
        <img src="../img/mesa/henrique.svg" alt="Prof. Henrique Fontenele" class="integrante henri">

      </article>

      <article>
        <h1 class="titulo-artigo">Palestras</h1>
        <hr class="linha-titulo">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Título</th>
              <th scope="col">Palestrante</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Planejadores Clássicos e a nova era da Inteligência Artificial: planejando tarefas com VANTs</td>
              <td>Prof. Dr. Ricardo Sekeff </td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Mulheres, ciência e tecnologia</td>
              <td>Ana Maria Gomes</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Algo mais sobre as startups</td>
              <td>Prof. Dr. Gildário Lima</td>
            </tr>
            <tr>
              <th scope="row">4</th>
              <td>Algoritmo baseado na teoria da percolação para o reconhecimento de padrões em imagens da topografia cortical</td>
              <td>Prof. Me. F. Gerson Meneses</td>
            </tr>
            <tr>
              <th scope="row">5</th>
              <td>Python é isso tudo mesmo?</td>
              <td>Gustavo Carvalho</td>
            </tr>
          </tbody>
        </table>
      </article>

      <article>
        <h1 class="titulo-artigo">Minicursos</h1>
        <hr class="linha-titulo">

        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Título</th>
              <th scope="col">Ministrante</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Desenvolvendo Bots para Telegram com Python</td>
              <td>Luiz Rodrigo Gonzaga</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Prática e Conceitos do Desenvolvimento Mobile Usando Tecnologias Nativas e Híbridas</td>
              <td>João Patrick Rodrigues</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Desenvolvimento de Aplicações para Desktop com Electron</td>
              <td>Daniel Farias</td>
            </tr>
            <tr>
              <th scope="row">4</th>
              <td>Introdução à Programação Web com Python 3 e Django</td>
              <td>Werberth Vinicius</td>
            </tr>
            <tr>
              <th scope="row">5</th>
              <td>Desenvolvendo Jogos com Python e PyGame</td>
              <td>João Paulo Carvalho <strong>&</strong> Lissa Ximenes</td>
            </tr>
            <tr>
              <th scope="row">6</th>
              <td>Descobrindo o mundo das APIs</td>
              <td>Patrick Mazulo</td>
            </tr>
          </tbody>
        </table>
      </article>

      <article>
        <h1 class="titulo-artigo">Oficinas</h1>
        <hr class="linha-titulo">

        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Título</th>
              <th scope="col">Ministrante</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Introdução ao Arduino com Protótipos Controláveis</td>
              <td>Gustavo Brandão, Pedro Gomes <strong>&</strong> Augusto Silva</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Interfaces gráficas com Python 3</td>
              <td>Evelyn Silva</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Desenvolvendo Apps Mobile com Thunkable</td>
              <td>Lissa Ximenes</td>
            </tr>
            <tr>
              <th scope="row">4</th>
              <td>Construindo um Console Retro com Raspberry Pi</td>
              <td>Wendell Santos <strong>&</strong> Tawan Rodrigues</td>
            </tr>
            <tr>
              <th scope="row">5</th>
              <td>LaTex para a escrita de trabalhos acadêmicos</td>
              <td>Deusiane Oliveira</td>
            </tr>
            <tr>
              <th scope="row">6</th>
              <td>Tratamento de imagens com Python</td>
              <td>Pedro Cunha</td>
            </tr>
            <tr>
              <th scope="row">7</th>
              <td>Introdução a robótica utilizando a Plataforma Arduino</td>
              <td>Tiago Thales</td>
            </tr>
          </tbody>
        </table>
      </article>

        <span class="link-nav" id="lugar"></span>
        <article class="">
          <h1 class="titulo-artigo">Lugar</h1>
          <hr class="linha-titulo">
          <iframe width="100%" height="400" frameborder="0" style="border:0"
          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCiMVlPBM-_jxA1JXPRVM3SRDGztlGtDOM
          &q=Instituto+Federal+do+Piauí+-+Campus+Parnaíba" allowfullscreen>
        </iframe>
      </article>

    </section>

    <footer>
      <div class="footer-content">

        <div class="footer-subcontent">

          <img src="../img/ifpi-vertical.png" alt="" class="logo-ifpi">

          <article>
            <h1 class="titulo-rodape">Local do evento</h1>
            <p>Rodovia BR-402, Km 3, s/n</p>
            <p>Baixa do Aragão</p>
            <p>Parnaíba - PI</p>
            <p>64215-000</p>
          </article>

          <article>
            <h1 class="titulo-rodape">Contato</h1>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99539-1598</p>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99576-2787</p>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99585-6340</p>
            <p><i class="fas fa-envelope"></i> &nbsp; seifpi.phb@gmail.com</p>
          </article>

        </div>

        <hr>
        <p class="creditos">
          SEIFPI&copy; 2018 · Todos os direitos reservados · Desde 2011 · <i class="fas fa-code"></i> with <i class="fas fa-heart"></i> by <strong><a href="https://jjpaulo2.github.io" target="_blank">@jjpaulo2</a></strong>
        </p>

      </div>
    </footer>


  </body>
</html>
