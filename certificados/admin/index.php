<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ./login.php");
  }

  include_once(__DIR__ . "/../../style_include.php");

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Área restrita · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../img/icon.png">
    <script type="text/javascript" src="../../js/script.js"></script>
    <link rel="stylesheet" href="../../style.css">
    <link rel="stylesheet" href="./style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./gerenciar/atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./gerenciar/participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="./logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="corpo">

      <div class="container">

        <div class="row botoes">

          <div class="col-lg">
            <a href="./gerenciar/atividades.php" class="btn btn-info btn-block"><i class="fas fa-bookmark"></i> Atividades</a>
          </div>
          <div class="col-lg">
            <a href="./gerenciar/participantes.php" class="btn btn-info btn-block"><i class="fas fa-users"></i> Participantes</a>
          </div>

        </div>

      </div>



    </div>

  </body>
</html>
