<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ../login.php");
  }

  include_once(__DIR__ . "/../../../style_include.php");

  include_once(__DIR__ . "/../../db/Conexao.php");
  include_once(__DIR__ . "/../../db/dao/ParticipanteDAO.php");

  $dao = new ParticipanteDAO();

  if(!empty($_POST["nome"])) {

    try {
      $dao->atualizar($_POST["edit"], $_POST["nome"], $_POST["tipo"]);
      header("Location: ../gerenciar/participantes.php?edit=" . explode(' ', $_POST["nome"])[0]);
    }
    catch(Exception $e) {
      header("Location: ?erro=1");
    }

  }
  else {

    if(empty($_GET["edit"])){
      header("Location: ../gerenciar/participantes.php");
    }
    else {
      $participante = $dao->getParticipanteInfo($_GET["edit"]);
    }

  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Cadastrar novo participante · Área restrita · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../../img/icon.png">
    <script type="text/javascript" src="../../../js/script.js"></script>
    <link rel="stylesheet" href="../../../style.css">
    <link rel="stylesheet" href="../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../gerenciar/atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="../gerenciar/participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="../logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <h1 class="titulo-artigo">Cadastrar participante</h1>
      <hr class="linha-titulo">

      <?php if (!empty($_GET["erro"])): ?>

        <div class="alert alert-danger">
          Erro ao atualizar informações do participante.
        </div>

      <?php endif; ?>

      <form class="" action="./participante.php" method="post">

        <input type="number" name="edit" value="<?php echo $_GET['edit']; ?>" style="display: none;">

        <div class="form-group">
          <label for="nome">Nome completo</label>
          <input type="text" value="<?php echo $participante['nome']; ?>" class="form-control" name="nome" id="nome" placeholder="Inserir o nome completo." required>
        </div>

        <div class="form-group">
          <label for="tipo">Tipo</label>
          <select class="form-control" id="tipo" name="tipo" required>

            <?php foreach ($dao->tiposPossiveis() as $tipo): ?>

              <option value="<?php echo $tipo['id']; ?>" <?php if($tipo['id']==$participante['tipo']) { echo " selected"; } ?> ><?php echo $tipo["tipo"]; ?></option>

            <?php endforeach; ?>

          </select>
        </div>

        <button type="submit" class="btn btn-success botao-cadastro" name="button">Atualizar</button>

      </form>

    </section>


  </body>
</html>
