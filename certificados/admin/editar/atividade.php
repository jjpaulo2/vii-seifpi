<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ../login.php");
  }

  include_once(__DIR__ . "/../../../style_include.php");

  include_once(__DIR__ . "/../../db/Conexao.php");
  include_once(__DIR__ . "/../../db/dao/AtividadeDAO.php");

  $dao = new AtividadeDAO();

  if(!empty($_POST["edit"])) {

    try {
      $dao->atualizar($_POST["edit"], $_POST["tipo"], $_POST["numero"], $_POST["titulo"], $_POST["carga"]);
      header('Location: ../gerenciar/atividades.php?edit=' . $_POST["numero"] . '&tipo=' . $_POST["tipo"]);
    }
    catch(Exception $e) {
      header("Location: ?erro=1");
    }

  }
  else {

    if(empty($_GET["edit"])){
      header("Location: ../gerenciar/atividades.php");
    }
    else {
      $atividade = $dao->getAtividadeInfo($_GET["edit"]);
    }

  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Cadastrar nova atividade · Área restrita · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../../img/icon.png">
    <script type="text/javascript" src="../../../js/script.js"></script>
    <link rel="stylesheet" href="../../../style.css">
    <link rel="stylesheet" href="../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../gerenciar/atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../gerenciar/participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="../logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <h1 class="titulo-artigo">Atualizar atividade</h1>
      <hr class="linha-titulo">

      <?php if (!empty($_GET["erro"])): ?>

        <div class="alert alert-danger">
          Erro ao editar atividades.
        </div>

      <?php endif; ?>

      <form action="./atividade.php" method="post">

        <input type="number" name="edit" value="<?php echo $_GET['edit']; ?>" style="display: none;">

        <div class="form-group">
          <label for="tipo">Tipo</label>
          <select class="form-control" id="tipo" name="tipo" required>

            <?php foreach ($dao->tiposPossiveis() as $tipo): ?>

              <option value="<?php echo $tipo['id']; ?>" <?php if($tipo["id"]==$atividade["id_tipo"]) { echo "selected"; } ?>><?php echo $tipo["tipo"]; ?></option>

            <?php endforeach; ?>

          </select>
        </div>

        <div class="row">
          <div class="col-lg">

            <div class="form-group">
              <label for="numero">Número</label>
              <input type="number" class="form-control" name="numero" id="numero" placeholder="Inserir o número da atividade." value="<?php echo $atividade['numero']; ?>" required>
            </div>

          </div>
          <div class="col-lg">

            <div class="form-group">
              <label for="carga">Carga horária</label>
              <input type="number" class="form-control" name="carga" id="carga" placeholder="Inserir a carga horária da atividade." value="<?php echo $atividade['carga_horaria']; ?>" required>
            </div>

          </div>
        </div>

        <div class="form-group">
          <label for="titulo">Título</label>
          <input type="text" class="form-control" name="titulo" id="titulo" placeholder="Inserir o título da atividade." value="<?php echo $atividade['titulo']; ?>" required>
        </div>

        <button type="submit" class="btn btn-success botao-cadastro" name="button">Atualizar</button>

      </form>

    </section>


  </body>
</html>
