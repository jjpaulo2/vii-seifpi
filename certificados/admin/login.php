<?php

  session_start();
  if(!empty($_SESSION["logado"])) {
    header("Location: ../admin/");
  }

  include_once(__DIR__ . "/../../style_include.php");

  include_once(__DIR__ . "/../db/Conexao.php");
  include_once(__DIR__ . "/../db/dao/AdminDAO.php");

  $dao = new AdminDAO();

  if(!empty($_POST["usuario"])) {
    if($dao->existeUsuario($_POST["usuario"])){

      if($dao->login($_POST["usuario"], $_POST["senha"])){

        $_SESSION["logado"] = true;
        header("Location: ../admin/");

      }
      else {
        header("Location: ?erro=2");
      }

    }
    else {
      header("Location: ?erro=1");
    }
  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Área restrita · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../img/icon.png">
    <script type="text/javascript" src="../../js/script.js"></script>
    <link rel="stylesheet" href="../../style.css">
    <link rel="stylesheet" href="./style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../page#sobre">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../page#confirmados">Confirmados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../page#cronograma">Cronograma</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../page#lugar">Lugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../../certificados/"><i class="fas fa-calendar-check"></i> Certificados</a>
          </li>
        </ul>
        <ul class="nav justify-content-end social">
          <li class="nav-item">
            <a class="nav-link" href="https://www.instagram.com/seifpi2018/" target="_blank"><i class="fab fa-instagram"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.facebook.com/viiseifpi/" target="_blank"><i class="fab fa-facebook"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="corpo">

      <?php if(!empty($_GET["erro"])): ?>

        <div class="alert alert-danger alert-dismissible fade show" role="alert">

          <?php

          switch($_GET["erro"]){
            case 1:
            echo "Usuário não existe em nossa base de dados.";
            break;
            case 2:
            echo "Senha incorreta!";
            break;
          }

          ?>

          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php endif; ?>

      <div class="login-box border rounded">

        <h1 class="formulario-titulo rounded">Área restrita</h1>

        <form class="" action="./login.php" method="post">

          <div class="form-group">
            <label for="usuario">Usuário</label>
            <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Insira o usuário">
          </div>
          <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password" class="form-control" name="senha" id="senha" placeholder="*********">
          </div>
          <button type="submit" class="btn btn-primary botao-entrar">Entrar</button>

        </form>

      </div>

    </div>



  </body>
</html>
