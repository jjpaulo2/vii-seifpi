<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ../../login.php");
  }

  include_once(__DIR__ . "/../../../../style_include.php");

  include_once(__DIR__ . "/../../../db/Conexao.php");
  include_once(__DIR__ . "/../../../db/dao/AtividadeDAO.php");
  include_once(__DIR__ . "/../../../db/dao/ParticipanteDAO.php");

  $atividadeDao = new AtividadeDAO();
  $participanteDao = new ParticipanteDAO();

  if(empty($_GET["atividade"])) {
    header("Location: ../atividades.php");
  }

  if(!empty($_GET["add"])) {

    $atividadeDao->inserirParticipante($_GET["atividade"], $_GET["add"]);
    header("Location: ../visualizar/atividade.php?id=" . $_GET["atividade"] . "&sucesso=1");

  }

  $atividade = $atividadeDao->getAtividadeInfo($_GET["atividade"]);

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Adicionar participante à atividade · Área restrita · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../../../img/icon.png">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <link rel="stylesheet" href="../../../../style.css">
    <link rel="stylesheet" href="../../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="../../logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <h2 class="subtitulo-artigo">Adicionar participantes</h2>
      <h2 class="subtitulo-artigo"><strong><?php echo $atividade["tipo"] . " " . $atividade["numero"]; ?></strong></h2>
      <hr class="linha-titulo">

      <form method="get">

        <input type="number" name="atividade" value="<?php echo $_GET['atividade']; ?>" style="display: none">

        <div class="container">
          <div class="row">

            <div class="col-lg-10">

              <div class="form-group">
                <input type="text" name="nome" class="form-control" id="nome" placeholder="Busque pelo seu nome" <?php if(!empty($_GET["nome"])) { echo "value=\"" . $_GET["nome"] ."\""; } ?>>
              </div>

            </div>
            <div class="col-lg">

              <button type="submit" class="btn btn-success botao-pesquisar">Pesquisar</button>

            </div>

          </div>
        </div>

      </form>

      <?php if (!empty($_GET["nome"])): ?>

        <?php foreach ($participanteDao->buscar($_GET["nome"]) as $participante): ?>

          <div class="alert alert-secondary">

            <?php echo $participante["nome"] . "<br><strong>" . $participante["tipo"] . "</strong>"; ?>
            <a href="?atividade=<?php echo $_GET["atividade"] . "&"; ?>add=<?php echo $participante["id"]; ?>" class="btn btn-secondary botao-editar"><i class="fas fa-user-plus"></i></a>

          </div>

        <?php endforeach; ?>

      <?php else: ?>

        <div class="alert alert-warning">
          Faça uma busca primeiro.
        </div>

      <?php endif; ?>

    </section>


  </body>
</html>
