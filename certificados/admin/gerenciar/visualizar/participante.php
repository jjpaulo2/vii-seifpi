<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ../../login.php");
  }

  include_once(__DIR__ . "/../../../../style_include.php");

  include_once(__DIR__ . "/../../../db/Conexao.php");
  include_once(__DIR__ . "/../../../db/dao/ParticipanteDAO.php");

  $dao = new ParticipanteDAO();

  if(empty($_GET["id"])) {
    header("Location: ../participantes.php");
  }

  $participante = $dao->getParticipanteInfo($_GET["id"]);


?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title><?php echo $participante["nome"]; ?> · Área restrita · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../../../img/icon.png">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <link rel="stylesheet" href="../../../../style.css">
    <link rel="stylesheet" href="../../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="../../logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <h1 class="subtitulo-artigo">
        <?php echo $participante["nome"]; ?>
      </h1>
      <h2 class="subtitulo-artigo">
        <strong><?php echo $participante["tipo"]; ?></strong>
      </h2>
      <hr class="linha-titulo">

      <div class="alert alert-secondary">
        VII Semana de Informática do IFPI Campus Parnaíba<br>
        <strong>Evento</strong>

        <a href="../../../certificado.php?tipo=1&participante=<?php echo $_GET['id']; ?>" class="btn btn-secondary botao-editar" title="gerar certificado"><i class="fas fa-file-pdf"></i></a>
      </div>

      <?php if ($participante["id_tipo"] == 1): ?>

        <div class="alert alert-secondary">
          VII Semana de Informática do IFPI Campus Parnaíba<br>
          <strong>Organização</strong>

          <a href="../../../certificado.php?tipo=2&participante=<?php echo $_GET['id']; ?>" class="btn btn-secondary botao-editar" title="gerar certificado"><i class="fas fa-file-pdf"></i></a>
        </div>

      <?php endif; ?>

      <?php if ($dao->participaDeAtividade($_GET["id"])): ?>

        <?php foreach ($dao->getAtividades($_GET["id"]) as $atividade): ?>

          <div class="alert alert-secondary">
            <?php echo $atividade["atividade"]; ?><br>
            <strong>
              <?php
              switch ($atividade["atividade_tipo"]) {
                case 1:
                echo "Palestra ";
                break;
                case 2:
                echo "Minicurso ";
                break;
                case 3:
                echo "Oficina ";
                break;
              }
              echo $atividade["atividade_numero"];
              ?>
            </strong>

            <a href="../../../certificado.php?tipo=<?php echo $atividade['atividade_tipo']+2; ?>&participante=<?php echo $participante['id']; ?>&atividade=<?php echo $atividade['id_atividade']; ?>" class="btn btn-secondary botao-editar" title="gerar certificado"><i class="fas fa-file-pdf"></i></a>

          </div>

        <?php endforeach; ?>

      <?php else: ?>

        <div class="alert alert-danger">
          Nenhuma atividade registrada para este participante.
        </div>

      <?php endif; ?>


    </section>

  </body>
</html>
