<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ../../login.php");
  }

  include_once(__DIR__ . "/../../../../style_include.php");

  include_once(__DIR__ . "/../../../db/Conexao.php");
  include_once(__DIR__ . "/../../../db/dao/AtividadeDAO.php");

  $dao = new AtividadeDAO();

  if(empty($_GET["id"])) {
    header("Location: ../atividades.php");
  }

  $atividade = $dao->getAtividadeInfo($_GET["id"]);

  if(!empty($_GET["remove"])){
    $dao->removerParticipante($_GET["remove"]);
    header("Location: ?id=" . $_GET["id"] . "&sucesso=2");
  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../../../img/icon.png">
    <script type="text/javascript" src="../../../../js/script.js"></script>
    <link rel="stylesheet" href="../../../../style.css">
    <link rel="stylesheet" href="../../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="../../logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <h1 class="subtitulo-artigo">
        <?php echo $atividade["titulo"]; ?>
      </h1>
      <h2 class="subtitulo-artigo">
        <strong><?php echo $atividade["tipo"] . " " . $atividade["numero"]; ?></strong>
      </h2>
      <hr class="linha-titulo">

      <?php if (!empty($_GET["sucesso"])): ?>

        <div class="alert alert-success alert-dismissible fade show" role="alert">

          <?php

            switch ($_GET["sucesso"]) {
              case 1:
                echo "Participante adicionado com sucesso!";
                break;
              case 2:
                echo "Participante removido com sucesso!";
                break;
            }

          ?>

          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php endif; ?>

      <a href="../adicionar/participante.php?atividade=<?php echo $atividade['id']; ?>" class="btn btn-success"><i class="fas fa-user-plus"></i> Adicionar participante</a>
      <br><br>

      <?php if ($dao->existemParticipantesCadastrados($_GET["id"])): ?>

        <?php foreach ($dao->getParticipantes($_GET["id"]) as $participante): ?>

          <div class="alert alert-secondary">

            <?php echo $participante["participante"]; ?> <br>
            <strong>
            <?php
              switch ($participante["participante_tipo"]) {
                case 1:
                  echo "Organizador";
                  break;
                case 2:
                  echo "Palestrante";
                  break;
                case 3:
                  echo "Ministrante";
                  break;
                case 4:
                  echo "Participante";
                  break;
              }
            ?>
            </strong>

            <a href="javascript: remover(<?php echo $participante["id_relacao"]; ?>)" class="btn btn-secondary botao-editar" title="remover participante"><i class="fas fa-user-minus"></i></a>

          </div>

        <?php endforeach; ?>

      <?php else: ?>

        <div class="alert alert-danger">
          Nenhum participante cadastrado nesta atividade.
        </div>

      <?php endif; ?>
    </section>

    <script type="text/javascript">

      function remover(id_relacao) {

        if(confirm("Tem certeza que deseja remover este participante da atividade?")) {

          window.location.href = location + "&remove=" + id_relacao;

        }

      }

    </script>

  </body>
</html>
