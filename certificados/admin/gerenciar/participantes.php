<?php

  session_start();
  if(empty($_SESSION["logado"])) {
    header("Location: ../login.php");
  }

  include_once(__DIR__ . "/../../../style_include.php");

  include_once(__DIR__ . "/../../db/Conexao.php");
  include_once(__DIR__ . "/../../db/dao/ParticipanteDAO.php");

  $dao = new ParticipanteDAO();

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Participantes cadastrados · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../../../img/icon.png">
    <script type="text/javascript" src="../../../js/script.js"></script>
    <link rel="stylesheet" href="../../../style.css">
    <link rel="stylesheet" href="../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../../../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../../../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./atividades.php"><i class="fas fa-bookmark"></i> Atividades</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="./participantes.php"><i class="fas fa-users"></i> Participantes</a>
          </li>

        </ul>
        <ul class="navbar-nav justify-content-end">
          <li class="nav-item">
            <a class="nav-link" href="../logoff.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <form method="get">

        <div class="container">
          <div class="row">

            <div class="col-lg-10">

              <div class="form-group">
                <input type="text" name="nome" class="form-control" id="nome" placeholder="Busque pelo nome do participante" <?php if(!empty($_GET["nome"])) { echo "value=\"" . $_GET["nome"] ."\""; } ?>>
              </div>

            </div>
            <div class="col-lg">

              <button type="submit" class="btn btn-success botao-pesquisar">Pesquisar</button>

            </div>

          </div>
        </div>

      </form>
      <hr>

      <?php if (!empty($_GET["novo"])): ?>

        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <strong><?php echo $_GET["novo"]; ?></strong> cadastrado com sucesso!
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php elseif (!empty($_GET["edit"])): ?>

        <div class="alert alert-success alert-dismissible fade show" role="alert">
          Informações de <strong><?php echo $_GET["edit"]; ?></strong> atualizadas com sucesso!
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php endif; ?>

      <a href="../cadastrar/participante.php" class="btn btn-success"><i class="fas fa-user-plus"></i> Cadastrar participante</a>
      <br><br>


      <?php if($dao->existemParticipantes()): ?>

        <?php if (!empty($_GET["nome"])): ?>

          <?php foreach ($dao->buscar($_GET["nome"]) as $participante): ?>

            <div class="alert alert-secondary">
              <?php echo $participante["nome"] . "<br><strong>" . $participante["tipo"] . "</strong>" ; ?>

              <a href="../editar/participante.php?edit=<?php echo $participante['id']; ?>" class="btn btn-secondary botao-editar"><i class="fas fa-user-edit"></i></a>
              <a href="./visualizar/participante.php?id=<?php echo $participante['id']; ?>" class="btn btn-secondary botao-editar"><i class="fas fa-eye"></i></a>
            </div>

          <?php endforeach; ?>

        <?php else: ?>

          <?php foreach ($dao->selecionarTodosPequeno() as $participante): ?>

            <div class="alert alert-secondary">
              <?php echo $participante["nome"] . "<br><strong>" . $participante["tipo"] . "</strong>" ; ?>

              <a href="../editar/participante.php?edit=<?php echo $participante['id']; ?>" class="btn btn-secondary botao-editar"><i class="fas fa-user-edit"></i></a>
              <a href="./visualizar/participante.php?id=<?php echo $participante['id']; ?>" class="btn btn-secondary botao-editar"><i class="fas fa-eye"></i></a>
            </div>

          <?php endforeach; ?>

        <?php endif; ?>

      <?php else: ?>

        <div class="alert alert-danger">
          Nenhum participante cadastrado ainda.
        </div>

      <?php endif; ?>

    </section>


  </body>
</html>
