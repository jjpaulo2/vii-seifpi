<?php

  class ParticipanteDAO {

    public function existemParticipantes(){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes LIMIT 1";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return sizeof($lista) > 0;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function selecionarTodos(){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes_view";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function selecionarTodosPequeno(){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes_view ORDER BY id DESC LIMIT 8";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function cadastrar($nome, $tipo){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "INSERT INTO participantes VALUES (DEFAULT, :nome, :tipo)";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":nome" => $nome,
          ":tipo" => $tipo
        ));

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function atualizar($id, $nome, $tipo){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "UPDATE participantes SET nome=:nome, tipo=:tipo WHERE id=:id";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":nome" => $nome,
          ":tipo" => $tipo,
          ":id" => $id
        ));

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function quantidadeResultadosBusca($nome){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT count(id) AS 'quantidade' FROM participantes_view WHERE nome LIKE :nome";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":nome" => "%".$nome."%"
        ));
        $lista = $statement->fetchAll();

        return $lista[0][0];

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function buscar($nome){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes_view WHERE nome LIKE :nome";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":nome" => "%".$nome."%"
        ));
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function tiposPossiveis() {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM tipo_participante";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function getParticipanteInfo($participante) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes_view WHERE id=:id";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":id" => $participante
        ));
        $lista = $statement->fetchAll();

        return $lista[0];

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function participaDeAtividade($participante) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades_participantes_view WHERE participante=:id LIMIT 1";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":id" => $participante
        ));
        $lista = $statement->fetchAll();

        return $lista > 0;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function getAtividades($participante) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades_participantes_view WHERE participante=:id ORDER BY id_relacao ASC";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":id" => $participante
        ));
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }


  }
