<?php

  class AtividadeDAO {

    public function existemAtividades(){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades LIMIT 1";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return sizeof($lista) > 0;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function selecionarTodas(){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades_view";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function selecionarTodasPequeno(){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades_view ORDER BY id DESC LIMIT 8";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function cadastrar($tipo, $numero, $titulo, $ch){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "INSERT INTO atividades VALUES (DEFAULT, :tipo, :numero, :titulo, :carga)";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":tipo" => $tipo,
          ":numero" => $numero,
          ":titulo" => $titulo,
          ":carga" => $ch
        ));

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function atualizar($id, $tipo, $numero, $titulo, $ch){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "UPDATE atividades SET tipo=:tipo, numero=:numero, titulo=:titulo, carga_horaria=:carga WHERE id=:id";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":tipo" => $tipo,
          ":numero" => $numero,
          ":titulo" => $titulo,
          ":carga" => $ch,
          ":id" => $id
        ));

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function tiposPossiveis() {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM tipo_atividade";

        $statement = $conexao->query($sql);
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function getParticipantes($atividade) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes_atividades_view WHERE atividade=:atividade";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":atividade" => $atividade
        ));
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function getAtividadeInfo($atividade) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades_view WHERE id=:atividade";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":atividade" => $atividade
        ));
        $lista = $statement->fetchAll();

        return $lista[0];

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function existemParticipantesCadastrados($atividade){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM participantes_atividades_view WHERE atividade=:atividade LIMIT 1";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":atividade" => $atividade
        ));
        $lista = $statement->fetchAll();

        return sizeof($lista) > 0;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function inserirParticipante($atividade_id, $participante_id){

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "INSERT INTO participante_atividade VALUES (DEFAULT, :atividade, :participante)";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":atividade" => $atividade_id,
          ":participante" => $participante_id
        ));

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function removerParticipante($id_relacao) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "DELETE FROM participante_atividade WHERE id=:id";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":id" => $id_relacao
        ));

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

    public function buscarPorTitulo($titulo) {

      try {

        $conexao = (new Conexao())->getConexao();
        $sql = "SELECT * FROM atividades_view WHERE titulo LIKE :titulo";

        $statement = $conexao->prepare($sql);
        $statement->execute(array(
          ":titulo" => "%" . $titulo . "%"
        ));
        $lista = $statement->fetchAll();

        return $lista;

      }
      catch(PDOException $e) {
        throw $e;
      }

    }

  }
