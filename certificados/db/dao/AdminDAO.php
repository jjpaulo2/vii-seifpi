<?php

  class AdminDAO {

    public function existeUsuario($usuario){

      $conexao = (new Conexao())->getConexao();
      $sql = "SELECT * FROM admin WHERE user=:usuario";

      $statement = $conexao->prepare($sql);
      $statement->execute(array(
        ":usuario" => $usuario
      ));

      $lista = $statement->fetchAll();

      return sizeof($lista) > 0;

    }

    public function getSenha($usuario) {

      $conexao = (new Conexao())->getConexao();
      $sql = "SELECT senha FROM admin WHERE user=:usuario";

      $statement = $conexao->prepare($sql);
      $statement->execute(array(
        ":usuario" => $usuario
      ));

      $lista = $statement->fetchAll();

      return $lista[0][0];

    }

    public function login($usuario, $senha) {

      $conexao = (new Conexao())->getConexao();
      $sql = "SELECT senha FROM admin WHERE user=:usuario";

      $statement = $conexao->prepare($sql);
      $statement->execute(array(
        ":usuario" => $usuario
      ));

      $lista = $statement->fetchAll();

      return password_verify($senha, $lista[0][0]);

    }

  }
