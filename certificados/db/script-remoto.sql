USE seifpico_certificados;

CREATE TABLE tipo_participante(
  id INT NOT NULL AUTO_INCREMENT,
  tipo VARCHAR(20),

  PRIMARY KEY(id)
);

CREATE TABLE participantes(
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(50),
  tipo INT,

  PRIMARY KEY(id),
  FOREIGN KEY(tipo) REFERENCES tipo_participante(id)
);

CREATE TABLE tipo_atividade(
  id INT NOT NULL AUTO_INCREMENT,
  tipo VARCHAR(20),

  PRIMARY KEY(id)
);

CREATE TABLE atividades(
  id INT NOT NULL AUTO_INCREMENT,
  tipo INT,
  numero INT,
  titulo VARCHAR(50),
  carga_horaria INT,

  PRIMARY KEY(id),
  FOREIGN KEY(tipo) REFERENCES tipo_atividade(id)
);

CREATE TABLE participante_atividade(
  id INT NOT NULL AUTO_INCREMENT,
  atividade INT,
  participante INT,

  PRIMARY KEY(id),
  FOREIGN KEY(atividade) REFERENCES atividades(id),
  FOREIGN KEY(participante) REFERENCES participantes(id)
);

INSERT INTO tipo_participante VALUES
(DEFAULT, "Organizador"),
(DEFAULT, "Palestrante"),
(DEFAULT, "Ministrante"),
(DEFAULT, "Participante");

INSERT INTO tipo_atividade VALUES
(DEFAULT, "Palestra"),
(DEFAULT, "Minicurso"),
(DEFAULT, "Oficina");

CREATE VIEW participantes_view AS
  SELECT participantes.id, participantes.nome, tipo_participante.tipo, tipo_participante.id AS "id_tipo"
  FROM participantes, tipo_participante
  WHERE participantes.tipo=tipo_participante.id;

CREATE VIEW atividades_view AS
  SELECT atividades.id, atividades.titulo, atividades.numero, tipo_atividade.tipo, atividades.carga_horaria
  FROM atividades, tipo_atividade
  WHERE atividades.tipo=tipo_atividade.id;

CREATE VIEW participantes_atividades_view AS
  SELECT participante_atividade.id AS "id_relacao", atividades.id AS "atividade", participantes.nome AS "participante", participantes.id AS "participante_id", participantes.tipo AS "participante_tipo"
  FROM participantes, atividades, participante_atividade
  WHERE participantes.id=participante_atividade.participante AND atividades.id=participante_atividade.atividade;

CREATE VIEW atividades_participantes_view AS
  SELECT participante_atividade.id AS "id_relacao", participantes.id AS "participante", atividades.tipo AS "atividade_tipo", atividades.numero AS "atividade_numero", atividades.titulo AS "atividade", atividades.id AS "id_atividade"
  FROM participantes, atividades, participante_atividade
  WHERE participantes.id=participante_atividade.participante AND atividades.id=participante_atividade.atividade;

CREATE TABLE admin(
  id INT NOT NULL AUTO_INCREMENT,
  user VARCHAR(10),
  senha VARCHAR(60),

  PRIMARY KEY(id)
);

INSERT INTO admin VALUES
(DEFAULT, "admin", "$2y$12$DZ3al84ccASH4xxjMDS/i.AyeceoUllo2DYFhI59SgtppZHj51bzW");
