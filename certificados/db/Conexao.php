<?php

  class Conexao {

    private $user;
    private $host;
    private $senha;
    private $banco;

    private $pdo;

    public function __construct($user="root", $host="localhost", $senha="123", $banco="certificados") {
      $this->user = $user;
      $this->host = $host;
      $this->senha = $senha;
      $this->banco = $banco;
      $this->conexao();
    }

    private function conexao() {
      try {
        $this->pdo = new PDO("mysql: host=$this->host; dbname=$this->banco", $this->user, $this->senha,
        array(
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
          PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        ));
      }
      catch(PDOException $e) {
        throw $e;
      }
    }

    public function getConexao() {
      return $this->pdo;
    }


  }
