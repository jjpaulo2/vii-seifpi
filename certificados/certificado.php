<?php

  setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
  date_default_timezone_set('America/Sao_Paulo');

  require_once(__DIR__ . "/../vendor/autoload.php");
  include_once("./db/Conexao.php");
  include_once("./db/dao/ParticipanteDAO.php");
  include_once("./db/dao/AtividadeDAO.php");

  $participanteDao = new ParticipanteDAO();
  $atividadeDao = new AtividadeDAO();

  $mpdf = new \Mpdf\Mpdf([
    'tempDir' => __DIR__ . '/tmp',
    'mode' => 'utf-8',
    'format' => 'A4-L'
  ]);

  # TIPO 1 - EVENTO
  # TIPO 2 - ORGANIZADOR
  # TIPO 3 - PALESTRA
  # TIPO 4 - MINICURSO
  # TIPO 5 - OFICINA


  $tipo = $_GET["tipo"];
  $participante = $participanteDao->getParticipanteInfo($_GET["participante"]);

  if ($tipo == 1) {
    $especifico = "participou da VII Semana de Informática do IFPI Campus Parnaíba nos dias 12, 13 e 14 de setembro de 2018, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 40 horas.";
  }
  elseif ($tipo == 2) {
    $especifico = "compôs a equipe de organização na VII Semana de Informática do IFPI Campus Parnaíba, que ocorreu nos dias 12, 13 e 14 de setembro de 2018, no Instituto Federal do Piauí, em Parnaíba-PI.";
  }
  elseif ($tipo == 3) {
    $atividade = $atividadeDao->getAtividadeInfo($_GET["atividade"]);
    if($participante["id_tipo"] == 2){
      $especifico = "palestrou na VII Semana de Informática do IFPI Campus Parnaíba, com o tema <strong>" . $atividade["titulo"] . "</strong>, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 1 hora.";
    }
    else {
      $especifico = "participou da palestra <strong>" . $atividade["titulo"] . "</strong> na VII Semana de Informática do IFPI Campus Parnaíba, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 1 hora.";
    }
  }
  elseif ($tipo == 4) {
    $atividade = $atividadeDao->getAtividadeInfo($_GET["atividade"]);
    if($participante["id_tipo"] == 3){
      $especifico = "ministrou o minicurso <strong>" . $atividade["titulo"] . "</strong> na VII Semana de Informática do IFPI Campus Parnaíba, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 4 horas.";
    }
    else {
      $especifico = "participou do minicurso <strong>" . $atividade["titulo"] . "</strong> na VII Semana de Informática do IFPI Campus Parnaíba, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 4 horas.";
    }
  }
  elseif ($tipo == 5) {
    $atividade = $atividadeDao->getAtividadeInfo($_GET["atividade"]);
    if($participante["id_tipo"] == 3){
      $especifico = "ministrou a oficina <strong>" . $atividade["titulo"] . "</strong> na VII Semana de Informática do IFPI Campus Parnaíba, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 1 hora.";
    }
    else {
      $especifico = "participou da oficina <strong>" . $atividade["titulo"] . "</strong> na VII Semana de Informática do IFPI Campus Parnaíba, no Instituto Federal do Piauí, em Parnaíba-PI, com carga horária de 1 hora.";
    }
  }

  $textoCompleto = "
  <link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet'>
  </head>
  <body>
  <br><br><br><br><br><br><br>
  <div class='box'>
  <p class='texto'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Certificamos que <strong>" . $participante["nome"] . "</strong>, " . $especifico . "</p>
  <p class='data'>Parnaíba - PI, " . strftime('%d de %B de %Y', strtotime('today')) . "</p>
  </div>
  ";

  $css = file_get_contents("certificado.css");

  $mpdf->WriteHTML($css, 1);
  $mpdf->WriteHTML($textoCompleto);
  $mpdf->Output("certificado.pdf", "D");
