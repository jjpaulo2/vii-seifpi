<?php

  include_once(__DIR__ . "/../style_include.php");

  include_once(__DIR__ . "/db/Conexao.php");
  include_once(__DIR__ . "/db/dao/ParticipanteDAO.php");

  $dao = new ParticipanteDAO();

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Gerar meu certificado · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="description" content="Gere aqui o seu certificado da 7ª edição da SEIFPI. Este é um evento organizado anualmente pelos alunos do curso Técnico em Informática do IFPI Campus Parnaíba.">
    <meta property="og:image" content="../img/previa.png"/>
    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../img/icon.png">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="stylesheet" href="../style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#sobre">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#confirmados">Confirmados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#cronograma">Cronograma</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#lugar">Lugar</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="../certificados/"><i class="fas fa-calendar-check"></i> Certificados</a>
          </li>
        </ul>
        <ul class="nav justify-content-end social">
          <li class="nav-item">
            <a class="nav-link" href="https://www.instagram.com/seifpi2018/" target="_blank"><i class="fab fa-instagram"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.facebook.com/viiseifpi/" target="_blank"><i class="fab fa-facebook"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <header>
      <div class="cabecalho-fundo"></div>
      <div class="cabecalho-conteudo">
        <img class="logo-seifpi" src="../img/logo-grande.png" alt="SEIFPI LOGO">
        <h2 class="titulo2">Semana de Informática</h2>
        <h1 class="titulo1">VII SEIFPI</h1>
        <h3 class="titulo3">IFPI Campus Parnaíba</h3>
        <h3 class="data">Dias 12, 13 e 14 de setembro de 2018</h3>
      </div>
    </header>


    <section id="barra-pesquisa">

      <article class="">

        <div class="titulo">
          <h1 class="titulo-artigo">Gerar meu certificado</h1>
          <a href="./admin/login.php" class="btn btn-light botao-login">Área restrita</a>
        </div>
        <hr class="linha-titulo">

        <form method="get" action="#barra-pesquisa">

          <div class="container">
            <div class="row">

              <div class="col-lg-10">

                <div class="form-group">
                  <input type="text" name="busca" class="form-control" id="nome" placeholder="Busque pelo seu nome" <?php if(!empty($_GET["busca"])) {echo "value=\"" . $_GET["busca"] . "\"";} ?>>
                </div>

              </div>
              <div class="col-lg">

                <button type="submit" class="btn btn-success botao-pesquisar">Pesquisar</button>

              </div>

            </div>
          </div>

        </form>

        <?php if (!empty($_GET["busca"])): ?>

          <div class="alert alert-light">
            <?php echo $dao->quantidadeResultadosBusca($_GET["busca"]); ?> resultado(s) localizado(s) em nossa base de dados.
          </div>

          <?php foreach ($dao->buscar($_GET["busca"]) as $participante): ?>

            <a href="./participante.php?id=<?php echo $participante['id']; ?>" class="btn btn-light btn-block" style="text-align: left; white-space: normal !important;"><?php echo $participante["nome"] . " <strong>(" . $participante["tipo"] . ")</strong>"; ?></a>

          <?php endforeach; ?>

        <?php endif; ?>

      </article>


    </section>


    <footer>
      <div class="footer-content">

        <div class="footer-subcontent">

          <img src="../img/ifpi-vertical.png" alt="" class="logo-ifpi">

          <article>
            <h1 class="titulo-rodape">Local do evento</h1>
            <p>Rodovia BR-402, Km 3, s/n</p>
            <p>Baixa do Aragão</p>
            <p>Parnaíba - PI</p>
            <p>64215-000</p>
          </article>

          <article>
            <h1 class="titulo-rodape">Contato</h1>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99539-1598</p>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99576-2787</p>
            <p><i class="fas fa-mobile-alt"></i> &nbsp; (86) 99585-6340</p>
            <p><i class="fas fa-envelope"></i> &nbsp; seifpi.phb@gmail.com</p>
          </article>

        </div>

        <hr>
        <p class="creditos">
          SEIFPI&copy; 2018 · Todos os direitos reservados · Desde 2011 · <i class="fas fa-code"></i> with <i class="fas fa-heart"></i> by <strong><a href="https://jjpaulo2.github.io" target="_blank">@jjpaulo2</a></strong>
        </p>

      </div>
    </footer>


  </body>
</html>
