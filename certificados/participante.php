<?php

  include_once(__DIR__ . "/../style_include.php");

  include_once(__DIR__ . "/db/Conexao.php");
  include_once(__DIR__ . "/db/dao/ParticipanteDAO.php");

  $dao = new ParticipanteDAO();

  $participante = $dao->getParticipanteInfo($_GET["id"]);

  if(empty($_GET['id'])) {
    header("Location: ../certificados");
  }

?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Certificados de <?php echo explode(" ",$participante["nome"])[0]; ?> · SEIFPI 2018 · VII Semana de Informática do IFPI Campus Parnaíba</title>

    <meta name="description" content="Gere aqui o seu certificado da 7ª edição da SEIFPI. Este é um evento organizado anualmente pelos alunos do curso Técnico em Informática do IFPI Campus Parnaíba.">
    <meta property="og:image" content="../img/previa.png"/>
    <meta name="keywords" content="seifpi,vii,ifpi,parnaiba,phb,informatica,computacao">
    <meta name="author" content="João Paulo Carvalho">

    <?php incluir_estilo(); ?>

    <link rel="icon" href="../img/icon.png">
    <script type="text/javascript" src="../js/script.js"></script>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="./admin/style.css">

  </head>
  <body>

    <nav class="navbar navbar-expand-md navbar-dark" id="menu" style="background-color: #248737;">
      <span class="navbar-brand">
        <img src="../img/logo.png" width="30" height="30" alt="">
        <span class="site-nome">VII SEIFPI</span>
      </span>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="../page"><i class="fas fa-home"></i> Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#sobre">Sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#confirmados">Confirmados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#cronograma">Cronograma</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../page#lugar">Lugar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../certificados/"><i class="fas fa-calendar-check"></i> Certificados</a>
          </li>
        </ul>
        <ul class="nav justify-content-end social">
          <li class="nav-item">
            <a class="nav-link" href="https://www.instagram.com/seifpi2018/" target="_blank"><i class="fab fa-instagram"></i></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://www.facebook.com/viiseifpi/" target="_blank"><i class="fab fa-facebook"></i></a>
          </li>
        </ul>
      </div>
    </nav>

    <section style="margin-top: 100px;">

      <h1 class="subtitulo-artigo">
        <?php echo $participante["nome"]; ?>
      </h1>
      <h2 class="subtitulo-artigo">
        <strong><?php echo $participante["tipo"]; ?></strong>
      </h2>
      <hr class="linha-titulo">

      <div class="alert alert-secondary">
        VII Semana de Informática do IFPI Campus Parnaíba<br>
        <strong>Evento</strong>

        <a href="./certificado.php?tipo=1&participante=<?php echo $_GET['id']; ?>" class="btn btn-secondary botao-editar" title="gerar certificado" target="_blank"><i class="fas fa-file-pdf"></i></a>
      </div>

      <?php if ($participante["id_tipo"] == 1): ?>

        <div class="alert alert-secondary">
          VII Semana de Informática do IFPI Campus Parnaíba<br>
          <strong>Organização</strong>

          <a href="./certificado.php?tipo=2&participante=<?php echo $_GET['id']; ?>" class="btn btn-secondary botao-editar" title="gerar certificado" target="_blank"><i class="fas fa-file-pdf"></i></a>
        </div>

      <?php endif; ?>

      <?php if ($dao->participaDeAtividade($_GET["id"])): ?>

        <?php foreach ($dao->getAtividades($_GET["id"]) as $atividade): ?>

          <div class="alert alert-secondary">
            <?php echo $atividade["atividade"]; ?><br>
            <strong>
              <?php
              switch ($atividade["atividade_tipo"]) {
                case 1:
                echo "Palestra ";
                break;
                case 2:
                echo "Minicurso ";
                break;
                case 3:
                echo "Oficina ";
                break;
              }
              echo $atividade["atividade_numero"];
              ?>
            </strong>

            <a href="./certificado.php?tipo=<?php echo $atividade['atividade_tipo']+2; ?>&participante=<?php echo $participante['id']; ?>&atividade=<?php echo $atividade['id_atividade']; ?>" class="btn btn-secondary botao-editar" title="gerar certificado" target="_blank"><i class="fas fa-file-pdf"></i></a>

          </div>

        <?php endforeach; ?>

      <?php else: ?>

        <div class="alert alert-danger">
          Nenhuma atividade registrada para este participante.
        </div>

      <?php endif; ?>

    </section>


    <p class="creditos" style="margin-bottom: 30px;">
      SEIFPI&copy; 2018 · Todos os direitos reservados · Desde 2011 · <i class="fas fa-code"></i> with <i class="fas fa-heart"></i> by <strong><a href="https://jjpaulo2.github.io" target="_blank">@jjpaulo2</a></strong>
    </p>


  </body>
</html>
