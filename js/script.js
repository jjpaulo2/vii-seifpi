$(document).scroll(function() {
  if ($(this).scrollTop() > 1) {
    document.getElementById('menu').style.boxShadow = '0px 0px 10px rgba(0,0,0,.3)';
  }
  else {
    document.getElementById('menu').style.boxShadow = 'none';
  }
});
